---
  title: What is Cloud native?
  description:  Learn about cloud native applications that leverage technologies like containers, Kubernetes, and microservices to run at unprecedented scale and stability.
  components:
    - name: topics-header
      data:
        title: What is Cloud native?
        block:
            - text: |
                  Learn about cloud native applications that leverage technologies like containers, Kubernetes, and microservices to run at unprecedented scale and stability.
    - name: 'topics-copy-block'
      data:
        header: Cloud native explained
        column_size: 10
        blocks:
            - text: |
                Cloud-native is a term used to describe applications that are built to run in a cloud computing environment. These applications are designed to be scalable, highly available, and easy to manage.

                By contrast, traditional solutions are often designed for on-premise environments and then adapted for the cloud. This can lead to sub-optimal performance and increased complexity.

                As enterprises move more of their workloads to the cloud, they increasingly looking for solutions that are cloud-native. Cloud-native solutions are designed from the ground up to take advantage of the unique characteristics of the cloud, such as scalability, elasticity, and agility.

    - name: 'topics-copy-block'
      data:
        header: Cloud native technologies
        column_size: 10
        blocks:
            - text: |
                Taking full advantage of the power of the cloud computing model and container orchestration, cloud native is an innovative way to build and run applications. Cloud native applications are built to run in the cloud, moving the focus away from machines to the actual service.

                Because cloud native applications are architectured using [microservices](/topics/microservices/) instead of a monolithic application structure, they rely on containers to package the application’s libraries and processes for deployment. Microservices allow developers to build deployable apps that are composed as individual modules focused on performing one specific service. This decentralization makes for a more resilient environment by limiting the potential of full application failure due to an isolated problem.

                Container orchestration tools, like [Kubernetes](/solutions/kubernetes/), allow developers to coordinate the way in which an application’s containers will function, including scaling and deployment.

                Cloud native app development requires a shift to a [DevOps](/topics/devops/) operating structure. This means development and operations teams will work much more collaboratively, leading to a faster and smoother production process.

                Using Cloud native to build your applications has a number of tangible benefits:

                *  Saves money by monitoring and scaling application resources through cloud orchestration, i.e. container schedulers
                *  Updates ship faster
                *  Aligns operations with business goals
                *  More time on business goals, less time on maintenance
    
    - name: 'topics-copy-block'
      data:
        header: Cloud native challenges
        column_size: 10
        blocks:
            - text: |
                The cloud-native movement has brought new challenges for developers, ops teams, and organizations as a whole. Cloud-native challenges include managing multiple versions of software across different cloud providers, scaling applications up and down quickly, and making sure all components work together seamlessly. 

                Challenges include:

                *  Managing complexity: as more services and components are added to the mix
                *  Dealing with ephemeral infrastructure: which can make debugging and troubleshooting difficult 
                *  Ensuring efficient use of resources: the pay-as-you-go model of the cloud can quickly get expensive
                
                
                The key to cloud-native development is to use tools like Kubernetes, Docker, and Terraform to automate deployment, configuration management, and infrastructure provisioning. Organizations need to be aware of these challenges and have the necessary strategies and solutions in place to address them as they arise.
    - name: 'topics-copy-block'
      data:
        header: Why should an enterprise transition to cloud-native
        column_size: 10
        blocks:
            - text: |
                Cloud-native applications are designed to be more resilient and scalable than traditional applications. This is because they use cloud-based services to store data, run applications, and access resources. By transitioning to cloud-native applications, an enterprise can improve its resilience and scalability. With cloud-native, enterprises can quickly adapt to changing market conditions and customer demands, while reducing their IT infrastructure costs.  

                Meaning that in addition to increased security and compliance capabilities, and better visibility into the applications and services that make up the enterprise, this approach can also save money by reducing the number of servers and software required.
    - name: 'topics-copy-block'
      data:
        column_size: 10
        header: The building blocks of cloud-native architecture
        blocks:
            - text: |
                ### Containers

                [Containers](https://about.gitlab.com/blog/2017/11/30/containers-kubernetes-basics/) are an [alternative way to package applications](https://searchitoperations.techtarget.com/tip/What-are-containers-and-how-do-they-work) versus building for VMs or physical servers directly. Containers can run inside of a virtual machine or on a physical server. Containers hold an application’s libraries and processes, but don't include an operating system, making them lightweight. In the end, fewer servers are needed to run multiple instances of an application which reduces cost and makes them easier to scale. Some other [benefits of containers](https://tsa.com/top-5-benefits-of-containerization/) include faster deployment, better portability and scalability, and improved security.


                ### Orchestrators

                Once the containers are set, an orchestrator is needed to get them running. Container orchestrators direct how and where containers run, fix any that go down and determine if more are needed. When it comes to container orchestrators, also known as schedulers, Kubernetes is the clear-cut[ market winner](https://about.gitlab.com/blog/2018/08/02/top-five-cloud-trends/).

                 ### Microservices

                The last main component of cloud-native computing is microservices. In order to make apps run more smoothly, they can be broken down into smaller parts, or microservices, to make them easier to scale based on load. Microservices infrastructure also makes it easier – and faster – for engineers to develop an app. Smaller teams can be formed and assigned to take ownership of individual components of the app’s development, allowing engineers to code without potentially impacting another part of the project.

                While public cloud services like AWS offer the opportunity to build and deploy applications easily, there are times when it makes sense to build your own infrastructure. A private or hybrid cloud solution is generally needed when sensitive data is processed within an application or industry regulations call for increased controls and security.


    - name: 'topics-copy-block'
      data:
        column_size: 10
        header: GitLab is the place to build cloud native applications
        blocks:
            - text: |
                Cloud native applications are the future of software development and GitLab is the best place to build these apps.

                When transitioning to cloud native applications, your team will see a large increase in the number of projects that need to be managed because of the required use of microservices. The surge in project volume calls for consistent and efficient application lifecycle management — this is where GitLab comes in.

                GitLab is an [DevOps platform](/solutions/devops-platform/), delivered as a single application. From issue tracking and source code management to CI/CD and monitoring, having it all in one place simplifies toolchain complexity and speeds up cycle times. With a [built-in container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html) and [Kubernetes integration](https://docs.gitlab.com/ee/user/project/clusters/index.html), GitLab makes it easier than ever to get started with containers and cloud native development.
              video:
                video_url: https://www.youtube.com/embed/jc5cY3LoOOI?start=98
    - name: 'topics-copy-block'
      data:
        column_size: 10
        header: Cloud Native for Business
        blocks:
            - text: |
                Cloud native applications use containers, microservices architecture, and container orchestration like Kubernetes. GitLab is designed for cloud native applications with tight Kubernetes integration.

                Businesses are shifting from traditional deployment models to cloud native applications in order to gain speed, reliability and scale.

                Learn more about how GitLab can power your cloud native development.
              video:
                video_url: https://www.youtube.com/embed/wtaOQY_ITvQ
    - name: topics-cta
      data:
        title: Start your cloud native transformation
        text: |
          Hear how Ask Media Group migrated from on-prem servers to the AWS cloud with GitLab tools and integrations. Join us and learn from their experience.
        column_size: 10
        cta_one:
          text: Save your spot
          link: /webcast/cloud-native-transformation/
          data_ga_name: Save your spot
          data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Related Resources
        column_size: 4
        grouped: true
        cards:
          - icon:
              name: webcast
              variant: marketing
              alt: Webcast Icon
            event_type: "Video"
            header: Cloud Native Webinar
            link_text: "Watch now"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-devops.png"
            href: "https://www.youtube.com/watch?v=jc5cY3LoOOI#t=1m38s"
            data_ga_name: Cloud native webinar
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blog"
            header: Cloud Native Development with GitLab
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
            href: /blog/2017/04/18/cloud-native-demo/
            data_ga_name: Cloud Native Development with GitLab
            data_ga_location: resource cards
          - icon:
              name: articles
              variant: marketing
              alt: Articles Icon
            event_type: "Blog"
            header: Create a CI/CD Pipeline with Auto Deploy
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-cicd.png"
            href: /blog/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/
            data_ga_name: Create a CI/CD Pipeline with Auto Deploy
            data_ga_location: resource cards
          - icon:
              name: articles
              variant: marketing
              alt: Articles Icon
            event_type: "Doc"
            header: Auto DevOps Documentation
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-gitlab.png"
            href: https://docs.gitlab.com/ee/topics/autodevops/index.html
            data_ga_name: Auto DevOps Documentation
            data_ga_location: resource cards
