/label ~"dex-status::triage" ~"dex-approval::2-standard"


## Step 1: What is changing in this MR?

Please describe the change and link any relevant issues.

## Step 2: Add the appropriate labels for triage

This label will have `dex-approval::2-standard` automatically applied, but please update it as follows. If deciding between two levels, go with the higher of the two:

1. **dex-approval::1-minor**
   1. *Example of the type of change:* typos or content changes.
      1. Anyone on the Digital Experience team can approve and merge.
      1. Once approved by the Digital Experience team, the MR creator can merge.
   1. *Example of the type of change:* /customers filter is displaying the wrong results.
      1. Anyone on the Digital Experience team can approve and merge.
      1. Once approved by the Digital Experience team, the MR creator can merge.
1. **dex-approval::2-standard**
   1. *Example of the type of change:* re-arranging components on a page.
      1. Once the Digital Experience team approves, members of the Digital Experience team can merge.
      1. Members of the Digital Experience team should keep their Managers informed.
      1. Manager’s discretion to align with the Director of Digital Experience or not.
   1. *Example of the type of change:* Adding new pages.
      1. Managers of Digital Experience can approve and merge.
      1. Manager should align with Director, Digital Experience.
1. **dex-approval::3-key-page**
   1. *Example of the type of change:* Changing Nav
      1. Directors and Managers of Digital Experience need to align.
      1. Managers of Digital Experience can approve and merge.
   1. *Example of the type of change:* Changes to key drivers of traffic and/or business goals etc. (Homepage, Pricing Page, Why GitLab).
      1. Director, Digital Experience can approve and merge.
      1. Director, Digital Experience will keep Marketing leadership informed of all Major Changes.
1. **dex-approval::4-legal**
   1. *Example of the type of change:* Any changes to company information, board/director information, etc.
      1. Legal and Director of Digital Experience must approve.
   1. *Example of the type of change:* Changes to sign-up workflows or alteration/omission of footers/links to anything legal-related.
      1. Legal and Director of Digital Experience must approve.

## Step 3: Tag the appropriate person for review

Depending on which label is used, you may tag the following people as a `Reviewer` on this MR:

1. **Level 1**: Any member of the [Digital Experience Team](https://about.gitlab.com/handbook/marketing/digital-experience/#groups-metrics--team-members)
1. **Level 2**: Any member of the [Digital Experience team](https://about.gitlab.com/handbook/marketing/digital-experience/#groups-metrics--team-members)
1. **Level 3**: Ping `@gitlab-com/marketing/digital-experience` in a comment. This will tag the [Digital Experience Leadership team](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/group_members?with_inherited_permissions=exclude), and they can review. When in doubt, tag `@mpreuss` as a reviewer. 
1. **Level 4**: This will need Legal approval. Tag `@mpreuss` and he can loop in the legal team. 
