---
  title: GitLab Partners
  description: Learn about GitLab partners, who they are, what services they provide, and how you can become a partner.
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab Partners
        subtitle: GitLab partners with global technology leaders to transform one platform into countless solutions.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: Apply today
          url: /
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: gitlab partners"
    - name: 'by-industry-intro'
      data:
        intro_text: Meet our Partners
        logos:
          - name: AWS
            image: /nuxt-images/partners/aws/aws-logo-black.svg
            aos_animation: zoom-in-up
            aos_duration: 200
            url: /partners/technology-partners/aws/
            aria_label: Link to AWS partner case study
          - name: GCP
            image: /nuxt-images/partners/gcp/gcp-logo-black.svg
            aos_animation: zoom-in-up
            aos_duration: 400
            url: /partners/technology-partners/google-cloud-platform/
            aria_label: Link to GCP partner case study
          - name: Hashicorp
            image: /nuxt-images/partners/hashicorp/hashicorp-logo-black.svg
            aos_animation: zoom-in-up
            aos_duration: 600
            url: /partners/technology-partners/hashicorp/
            aria_label: Link to Hashicorp partner case study
          - name: Redhat
            image: /nuxt-images/partners/redhat/redhat-logo-black.svg
            aos_animation: zoom-in-up
            aos_duration: 800
            url: /partners/technology-partners/redhat/
            aria_label: Link to Redhat partner case study
          - name: VMware Tanzu
            image: /nuxt-images/partners/vmware/vmware-logo-black.jpg
            aos_animation: zoom-in-up
            aos_duration: 1000
            url: /partners/technology-partners/vmware-tanzu/
            aria_label: Link to VMware Tanzu partner case study
          - name: IBM
            image: /nuxt-images/partners/ibm/ibm-logo-black.jpg
            aos_animation: zoom-in-up
            aos_duration: 1200
            url: /partners/technology-partners/ibm/
            aria_label: Link to IBM partner case study
    - name: 'topics-copy-block'
      data:
        header: Overview
        column_size: 8
        blocks:
          - text: |
              The GitLab Global Partner Program enables current and potential partners — including systems integrators, cloud platform partners, resellers, distributors, managed service providers, and ecosystem partners — to maximize the value of their DevOps expertise and the GitLab platform for their customers.

              Partners are a critical element of GitLab's mission to enable our customers with modern software-driven experiences, and to ensure "Everyone Can Contribute" through a robust and thriving partner ecosystem that cultivates innovation and stimulates transformation.

              Together, with our partners, we help enterprises of all sizes, all verticals, in over 65 countries globally, lead the digital transformation necessary to compete in today's rapidly shifting market.
    - name: 'topics-copy-block'
      data:
        header: Alliance Partners
        column_size: 8
        blocks:
          - text: |
              GitLab's mission is to create a platform and culture where "Everyone Can Contribute" and we firmly believe that partnerships are the foundation from which collaboration and innovation is built. 

              As a GitLab Alliance partner, you'll have access to the resources, tools and support you need to accelerate business growth providing greater customer value through solution offerings that integrate and interoperate with GitLab.
            link:
              url: https://partners.gitlab.com/English/register_email.aspx
              text: Become an Alliance Partner
              data_ga_name: become an alliance partner   
            secondary_link:
              url: https://about.gitlab.com/handbook/alliances/
              text: Learn more
              data_ga_name: learn more about alliances           
    - name: 'partners-showcase'
      data:
        items:
          - title: Align your company and technology with GitLab as the DevOps Leader
            icon:
              name: report
              alt: Automated Code Icon
              variant: marketing
          - title: Increase the value of your solution to your customers via deeper technical integrations in order to satisfy DevOps use cases
            icon:
              name: increase
              alt: Community Icon
              variant: marketing
          - title: Joint GTM activities to reach the modern DevOps market
            icon:
              name: report
              alt: Manage Icon
              variant: marketing
          - title: Access to sales and marketing resources to create assets for growing your business
            icon:
              name: report
              alt: Manage Icon
              variant: marketing  
    - name: 'topics-copy-block'
      data:
        header: Channel Partners
        column_size: 8
        blocks:
          - text: |
              GitLab’s global sales and integration partners help customers achieve technical and business goals in digital transformation. Channel partners resell or purchase licenses on behalf of end users and/or deliver deployment, integration, optimization, training and other services for GitLab customers. 
            link:
              url: https://partners.gitlab.com/English/register_email.aspx
              text: Become an Channel Partner
              data_ga_name: become an alliance partner
            secondary_link:
              url: https://about.gitlab.com/handbook/alliances/
              text: Learn more
              data_ga_name: learn more about alliances   
    - name: 'partners-showcase'
      data:
        items:
          - title: Reseller
            text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            icon:
              name: agile
              alt: Handshake Icon
              variant: marketing
          - title: Integrator
            text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            icon:
              name: agile
              alt: Handshake Icon
              variant: marketing
          - title: MSP
            text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            icon:
              name: agile
              alt: Handshake Icon
              variant: marketing
          - title: Training Partner
            text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            icon:
              name: agile
              alt: Handshake Icon
              variant: marketing                                              
    - name: 'get-started-resources'
      data:
        title: Partner Resources
        background: purple
        cards:
          - title: GitLab Docs
            icon:
              name: articles
              alt: Gitlab Docs Icon
            description: Documentation for GitLab Community Edition, GitLab Enterprise Edition, Omnibus GitLab, and GitLab Runner.
            link:
              text: Visit Docs
              url:  https://docs.gitlab.com/
              data_ga_name: visit docs
              data_ga_location: body
          - title: Developer Portal
            icon:
              name: code-alt-2
              alt: Developer Portal Icon
            description: Documentation for contributors to the GitLab Project -- information about our codebase, APIs, webhooks, design system, UI framework, and more!
            link:
              text: See Developer Portal
              url: https://developer.gitlab.com/
              data_ga_name: developer portal
              data_ga_location: body              
          - title: Blog
            icon:
              name: doc-pencil
              alt: Blog Icon          
            description: Visit the GitLab blog to learn about releases, applications, contributions, news, events, and more.
            link:
              text: Read the Blog
              url: /blog/
              data_ga_name: read the blog
              data_ga_location: body                  
